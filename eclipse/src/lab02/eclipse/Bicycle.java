package lab02.eclipse;

//William Trudel 1939241

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer,int numberGears,double maxSpeed) {
		this.manufacturer=manufacturer;
		this.numberGears=numberGears;
		this.maxSpeed=maxSpeed;
		
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public int getNumberGears() {
		return numberGears;
	}
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	@Override
	public String toString() {
		return "Bicycle manufacturer: " + manufacturer + ", Number of gears: " + numberGears + ", Max speed: " + maxSpeed;
	}
	

}
