package lab02.eclipse;

public class BikeStore {

	public static void main(String[] args) {
		
		Bicycle[] bike=new Bicycle[4];
		bike[0]=new Bicycle("Bike-0",54,33);
		bike[1]=new Bicycle("Just-a-Motorcycles",32,232);
		bike[2]=new Bicycle("Stinky",5,3);
		bike[3]=new Bicycle("Bike-0",42,56);
		
		for(int i=0;i<4;i++) {
			System.out.println(bike[i].toString());
		}
	}

}
